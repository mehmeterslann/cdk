import * as cdk from '@aws-cdk/core';
import * as ecr from '@aws-cdk/aws-ecr';
import * as ec2 from "@aws-cdk/aws-ec2";
import * as ecs from "@aws-cdk/aws-ecs";
import * as iam from '@aws-cdk/aws-iam';

export class CdkStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    const repository = new ecr.Repository(this, 'go-repository');
    const vpc = new ec2.Vpc(this, "Test-Vpc", {
      maxAzs: 3 // Default is all AZs in region
      
    });

    const cluster = new ecs.Cluster(this, "Test-Cluster", {
      vpc: vpc,
      clusterName:"Test-Cluster"
    });
    const role = new iam.Role(this, 'EcsRole', {
      assumedBy: new iam.ServicePrincipal('ecs-tasks.amazonaws.com'), 
      roleName: "EcsRole"
    });
    role.addToPolicy(new iam.PolicyStatement({
      effect: iam.Effect.ALLOW,
      resources: ["*"],
      actions: ['ecr:GetAuthorizationToken', 'ecr:BatchCheckLayerAvailability','ecr:GetDownloadUrlForLayer','ecr:BatchGetImage','logs:CreateLogStream','logs:PutLogEvents'],
      }));
    
    const task_definition = new ecs.FargateTaskDefinition(this,'Test-FargateService-task-definition',{
      executionRole: role,
      family: 'Test-FargateService-task-definition'
    });
    const container= task_definition.addContainer("test",{
      image:ecs.ContainerImage.fromRegistry("amazon/amazon-ecs-sample"),
    });
    const fargate= new ecs.FargateService(this,"test-service",{
      cluster:cluster,
      taskDefinition:task_definition,
      serviceName:"test-service"
    });
  }
}
